var mysql = require('mysql');

var connection = mysql.createConnection({
	host        : 'localhost',
	user        : 'root',
	password    : 'password',
	database    : 'your_database_name'
});

//If you don't want your password to be compromised - use this tutorial to export your password into enviroment variable: https://www.youtube.com/watch?v=-zmQat4iphg&ab_channel=IanSchoonover

/* Simple sequel query for example */
var q = 'SELECT CURDATE()';

connection.query(q, function (error, results, fields) {
	if(error) throw error;
	console.log(results);
})

connection.end();

